# SWGReckoning Core3 Code Repository #

[http://www.swgreckoning.com](http://www.swgreckoning.com)

### About This Repository ###

SWGReckoning is powered by SWGEmu's Core3 Star Wars Galaxies Emulator.

This repository contains the Core3 source code that SWGReckoning uses on its play server. Anyone is free to contribute via pull request, copy, modify, use, compile, or distribute this code.

### Important Information ###

Those that use the SWGReckoning Core3 source code contained in this repository are bound to the GNU AFFERO GENERAL PUBLIC LICENSE.

[https://www.gnu.org/licenses/agpl-3.0.html](https://www.gnu.org/licenses/agpl-3.0.html)


### How to compile and run ###
[http://swgreckoning.com/showthread.php?428-CentOS-6-6-x86_64-SWGEmu-Core3-Compile-Guide](http://swgreckoning.com/showthread.php?428-CentOS-6-6-x86_64-SWGEmu-Core3-Compile-Guide)